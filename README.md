
# Introduction

**Note: The wharf project is in early alpha stage!**

Wharf is a unified dashboard and workflow for devloping, deploying, and maintaining containerized applications. It supports common container engines like docker, rocket, and LXC.

_TODO: Feature List_

_TODO: Figure 1: Workflow_

The wharf project is still under heavy development. Expect bugs, design and API changes as we bring it to a stable, production product over the coming several months.

# Build wharf from source

```console
git clone https://github.com/dockercn/wharf.git $GOPATH/src/githhub.com/dockercn
cd $GOPATH/src/githhub.com/dockercn
go get -u github.com/astaxie/beego
go get -u github.com/codegangsta/cli
go get -u github.com/siddontang/ledisdb/ledis
go get -u github.com/garyburd/redigo/redis
go get -u github.com/shurcooL/go/github_flavored_markdown
go get -u github.com/satori/go.uuid
go get -u github.com/nfnt/resize
go build
```

# How to run wharf

Wharf can be run standalone or behind nginx.

## Running standalone

Step 1: Create and edit the wharf config file:

```console
touch wharf/conf/bucket.conf
```

Step 2: Copy and paste below lines into the config file:

```cfg
# set the running environment: development (`dev`) or production (`prod`).
runmode = dev

# Running wharf with TLS.
enablehttptls = true
# Set HTTPS port and
httpsport = 443
# Set the certificate and private key file.
httpcertfile = cert/containerops.me/containerops.me.crt
httpkeyfile = cert/containerops.me/containerops.me.key

gravatar = data/gravatar

[docker]
# Store docker or rocket images file here.
BasePath = /tmp/registry
StaticPath = files
# !!IMPORTANT!!
Endpoints = containerops.me
Version = 0.8.0
Config = prod
Standalone = true
OpenSignup = false

[ledisdb]
DataDir = /tmp/ledisdb
DB = 8

[log]
FilePath = /tmp
FileName = containerops.log

[session]
Provider = ledis
SavePath = /tmp/session
```

Step 3: Save the config file and run wharf:

```console
./wharf web --address 0.0.0.0 --port 80
```

## Running behind nginx

Step 1: Set `enablehttptls` parameter to `false` in the `wharf/conf/bucket.conf` file.

Step 2: Run wharf with http, such as

```console
./wharf web --address 127.0.0.1 --port 9911
```

Step 3: Copy the certificate and private key files to the `/etc/nginx` directory:

```console
sudo cp cert/containerops.me/* /etc/nginx
```

Step 4: Edit the nginx config file:

```nginx
upstream wharf_upstream {
  server 127.0.0.1:9911;
}

server {
  listen 80;
  server_name containerops.me;
  rewrite  ^/(.*)$  https://containerops.me/$1  permanent;
}

server {
  listen 443;

  server_name containerops.me;

  access_log /var/log/nginx/containerops-me.log;
  error_log /var/log/nginx/containerops-me-errror.log;

  ssl on;
  ssl_certificate /etc/nginx/containerops.me.crt;
  ssl_certificate_key /etc/nginx/containerops.me.key;

  client_max_body_size 1024m;
  chunked_transfer_encoding on;

  proxy_redirect     off;
  proxy_set_header   X-Real-IP $remote_addr;
  proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header   X-Forwarded-Proto $scheme;
  proxy_set_header   Host $http_host;
  proxy_set_header   X-NginX-Proxy true;
  proxy_set_header   Connection "";
  proxy_http_version 1.1;

  location / {
    proxy_pass         http://wharf_upstream;
  }
}
```

Step 5: Restart nginx service:

```console
sudo service nginx restart
```

# Using wharf

_TODO: Complete user manual_

1. Add your hosts file entry, for example: `192.168.1.66 containerops.me`.
2. Visit `https://containerops.me/auth` with a web browser and registrate a new user account, say, `wharfman`.
3. Run `docker login containerops.com` and login as `wharfman`.
4. _TODO: docker push_
5. _TODO: docker pull_

# Issue Submission

 Click [here](https://github.com/dockercn/wharf/search?q=&type=Issues) to search/post issues in this repository.

# Team

* Meaglith Ma [https://twitter.com/genedna](https://twitter.com/genedna)
* Allen Chen [https://github.com/chliang2030598](https://github.com/chliang2030598)
* Leo Meng [https://github.com/fivestarsky](https://github.com/chliang2030598)
* Unknwon [https://twitter.com/joe2010xtmf](https://github.com/chliang2030598)

# License

Wharf is released under [the MIT license](http://opensource.org/licenses/MIT).

# Other wharf releated projects

* [Vessel](https://github.com/dockercn/vessel): A continuous integration system built on top of Docker.
* [Rudder](https://github.com/dockercn/rudder): A golang client for the Docker remote API.

